CREATE OR REPLACE TABLE "flatten_source" AS
SELECT
    parse_json("value"):Rows AS obj_qbo_table
FROM
    "TransactionList"
;

CREATE OR REPLACE TABLE "stg01_fTransactionList" AS
SELECT 
    VALUE:ColData[0].value::VARCHAR AS "date"
    ,VALUE:ColData[1].value::VARCHAR AS "txn_type"
    ,VALUE:ColData[2].value::VARCHAR AS "doc_num"
    ,VALUE:ColData[3].value::VARCHAR AS "is_no_post"
    ,VALUE:ColData[4].value::VARCHAR AS "name"
    ,VALUE:ColData[5].value::VARCHAR AS "dept_name"
    ,VALUE:ColData[6].value::VARCHAR AS "memo"
    ,VALUE:ColData[7].value::VARCHAR AS "account_name"
    ,VALUE:ColData[8].value::VARCHAR AS "other_account"
    ,VALUE:ColData[9].value::VARCHAR AS "subt_nat_amount"
FROM "flatten_source",
    LATERAL FLATTEN( INPUT => obj_qbo_table:Row) f
;