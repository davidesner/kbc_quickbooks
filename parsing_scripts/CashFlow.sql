------------------------------------------------------------------------
-- FIRST SECTION
CREATE OR REPLACE TABLE "stage_1" AS
SELECT
    parse_json("value"):Rows AS obj_qbo_table
FROM
    "CashFlow"
;

CREATE OR REPLACE TABLE "stage_1_flatten" AS
SELECT
    --VALUE:Header:ColData[0]:value::VARCHAR AS "Section_1"
    VALUE:group::VARCHAR AS "Section_1"
    ,VALUE as "value"
FROM "stage_1",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE VALUE:Rows IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_1_DATA" AS
SELECT
    VALUE:group::VARCHAR AS "Section_1"
    ,CASE
        WHEN VALUE:Summary:ColData[0]:value::VARCHAR IS NOT NULL THEN VALUE:Summary:ColData[0]:value::VARCHAR
        ELSE VALUE:ColData[0]:value::VARCHAR
        END AS "Account"
    ,CASE
        WHEN VALUE:Summary:ColData[1]:value::VARCHAR IS NOT NULL THEN VALUE:Summary:ColData[1]:value::VARCHAR
        ELSE VALUE:ColData[1]:value::VARCHAR
        END AS "Money"
FROM "stage_1",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE VALUE:Rows IS NULL
;

------------------------------------------------------------------------
-- SECOND SECTION
CREATE OR REPLACE TABLE "stage_2" AS
SELECT
    "Section_1"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_1_flatten"
;

CREATE OR REPLACE TABLE "stage_2_flatten" AS
SELECT
    "Section_1"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_2"
    ,VALUE as "value"
FROM "stage_2",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_2" IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_2_DATA" AS
SELECT
    "Section_1"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_2"
    ,VALUE:ColData[0].value::VARCHAR AS "Account"
    ,VALUE:ColData[1].value::VARCHAR AS "Money"
FROM "stage_2",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_2" IS NULL
;

------------------------------------------------------------------------
-- THIRD SECTION
CREATE OR REPLACE TABLE "stage_3" AS
SELECT
    "Section_1"
    ,"Section_2"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_2_flatten"
;

CREATE OR REPLACE TABLE "stage_3_DATA" AS
SELECT 
    "Section_1"
    ,"Section_2"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_3"
    ,VALUE:ColData[0].value::VARCHAR AS "Account"
    ,VALUE:ColData[1].value::VARCHAR AS "Money"
FROM "stage_3",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_3" IS NULL
;

------------------------------------------------------------------------
-- CONCATENATING ALL DATA
CREATE OR REPLACE TABLE "stg01_CashFlow" AS
SELECT 
    "Section_1"
    ,'' AS "Section_2"
    ,"Account"
    ,"Money"
FROM "stage_1_DATA"
UNION
SELECT 
    "Section_1"
    ,'' AS "Section_2"
    ,"Account"
    ,"Money"
FROM "stage_2_DATA"
UNION
SELECT
    "Section_1"
    ,"Section_2"
    ,"Account"
    ,"Money"
FROM "stage_3_DATA"
;