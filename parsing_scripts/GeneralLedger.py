
import pandas as pd
import json

files = [
    'GeneralLedger_cash.csv',
    'GeneralLedger_accrual.csv'
]

for file in files:
    data = pd.read_csv('in/tables/{}'.format(file))

    data_json = json.loads(data['value'][0])

    out_columns = []
    for column in data_json['Columns']['Column']:
        out_columns.append(column['ColType'])

    data_out = []
    for row in data_json['Rows']['Row']:
        if 'Rows' in row:
            for sub_row in row['Rows']['Row']:
                sub_row_data = {}
                i = 0
                if 'ColData' in sub_row:
                    while i < len(out_columns):
                        sub_row_data[out_columns[i]
                                     ] = sub_row['ColData'][i]['value']
                        i += 1
                    data_out.append(sub_row_data)

    data_out_df = pd.DataFrame(data_out)
    data_out_df.to_csv('out/tables/out_{}'.format(file), index=False)
