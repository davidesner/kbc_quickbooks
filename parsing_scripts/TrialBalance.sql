------------------------------------------------------------------------
-- FIRST SECTION
CREATE OR REPLACE TABLE "stage_1" AS
SELECT
    parse_json("value"):Rows AS obj_qbo_table
FROM
    "TrialBalance"
;
CREATE OR REPLACE TABLE "stg01_TrailBalance" AS
SELECT
    VALUE:ColData[0].value::VARCHAR as "Account"
    ,VALUE:ColData[1].value::VARCHAR as "Debit"
    ,VALUE:ColData[2].value::VARCHAR as "Credit"
FROM "stage_1",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
;